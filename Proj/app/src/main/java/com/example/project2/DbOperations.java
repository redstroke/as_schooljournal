package com.example.project2;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.Collections;

public class DbOperations extends SQLiteOpenHelper {
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "students.db";
    private static final String CREATE_STUDENT_TABLE = "create table if not exists " +
            Student.StudentContract.StudentEntry.TABLE_NAME + "(" +
            Student.StudentContract.StudentEntry.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            Student.StudentContract.StudentEntry.FIO + " TEXT ," +
            Student.StudentContract.StudentEntry.FACULTY + " INTEGER NOT NULL " + "," +
            Student.StudentContract.StudentEntry.GROUP + " TEXT ," +
            Student.StudentContract.StudentEntry.SUBJECTS + " TEXT, " +
            Student.StudentContract.StudentEntry.TELEPHONE + " TEXT " + ");";

    DbOperations(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
        Log.d("Database operations", "Database created...");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_STUDENT_TABLE);
        Log.d("Database operations", "Tables created...");
    }

    public void addInfo(SQLiteDatabase db, Student student) {
        if(existStudent(db, student)) {
            updateStudent(db, student);
            Log.d("Database operations", "One row updated...");
        } else {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            ContentValues contentValues = new ContentValues();
            System.out.println("student from json addInfo " + student);
            contentValues.put(Student.StudentContract.StudentEntry.FACULTY, student.getFacultet());
            contentValues.put(Student.StudentContract.StudentEntry.SUBJECTS, gson.toJson(student.getSubjects()));
            contentValues.put(Student.StudentContract.StudentEntry.FIO, student.getFIO());
            contentValues.put(Student.StudentContract.StudentEntry.GROUP, student.getGroupe());
            contentValues.put(Student.StudentContract.StudentEntry.TELEPHONE, student.getNumPhone());
            db.insert(Student.StudentContract.StudentEntry.TABLE_NAME, null, contentValues);
            Log.d("Database operations", "One row inserted...");
        }
    }
    public void deleteStudent(SQLiteDatabase db, Student student) {
        if(existStudent(db, student)) {
            db.delete(
                    Student.StudentContract.StudentEntry.TABLE_NAME,
                    Student.StudentContract.StudentEntry.ID + " = ?",
                    new String[]{String.valueOf(student.getId())});
        }
    }
    public void updateStudent(SQLiteDatabase db, Student student) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        ContentValues contentValues = new ContentValues();
        contentValues.put(Student.StudentContract.StudentEntry.FACULTY, student.getFacultet());
        contentValues.put(Student.StudentContract.StudentEntry.SUBJECTS, gson.toJson(student.getSubjects()));
        contentValues.put(Student.StudentContract.StudentEntry.FIO, student.getFIO());
        contentValues.put(Student.StudentContract.StudentEntry.GROUP, student.getGroupe());
        contentValues.put(Student.StudentContract.StudentEntry.TELEPHONE, student.getNumPhone());
        db.update(Student.StudentContract.StudentEntry.TABLE_NAME, contentValues, Student.StudentContract.StudentEntry.ID + " = ?", new String[] {String.valueOf(student.getId())});
    }

    public void getAllStudents(SQLiteDatabase db) {
        String[] projections = {
                Student.StudentContract.StudentEntry.ID,
                Student.StudentContract.StudentEntry.FIO,
                Student.StudentContract.StudentEntry.SUBJECTS,
                Student.StudentContract.StudentEntry.FACULTY,
                Student.StudentContract.StudentEntry.TELEPHONE,
                Student.StudentContract.StudentEntry.GROUP
        };
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        MainActivity.mStudents.clear();
        //String selection = Student.StudentContract.StudentEntry.FACULTY + "= ?";
        //String [] selectionArgs = new String[] {faculty};
        Cursor cursor = db.query(Student.StudentContract.StudentEntry.TABLE_NAME, projections,
                null,null,null,null,null);
        while(cursor.moveToNext()) {
            @SuppressLint("Range") int id = cursor.getInt(
                    cursor.getColumnIndex(Student.StudentContract.StudentEntry.ID));
            @SuppressLint("Range") String fio = cursor.getString(
                    cursor.getColumnIndex(Student.StudentContract.StudentEntry.FIO));
            @SuppressLint("Range") String faculty = cursor.getString(
                    cursor.getColumnIndex(Student.StudentContract.StudentEntry.FACULTY));
            @SuppressLint("Range") ArrayList<Subject> subjects = gson.fromJson(
                    cursor.getString(
                            cursor.getColumnIndex(
                                    Student.StudentContract.StudentEntry.SUBJECTS)),
                    new TypeToken<ArrayList<Subject>>() {
                    }.getType()
            );
            @SuppressLint("Range") String telephone = cursor.getString(
                    cursor.getColumnIndex(Student.StudentContract.StudentEntry.TELEPHONE));
            @SuppressLint("Range") String group = cursor.getString(
                    cursor.getColumnIndex(Student.StudentContract.StudentEntry.GROUP));
            Student student = new Student(fio, faculty, group, telephone);
            student.setSubjects(subjects);
            student.setId(id);
            MainActivity.mStudents.add(student);
        }
    }

    public boolean existStudent(SQLiteDatabase db, Student student) {
        String[] projections = {
                Student.StudentContract.StudentEntry.ID
        };
        String selection = Student.StudentContract.StudentEntry.ID + "= ?";
        String [] selectionArgs = new String[] {String.valueOf(student.getId())};
        Cursor cursor = db.query(Student.StudentContract.StudentEntry.TABLE_NAME,
                projections,selection,selectionArgs,
                null,null,null );
        return cursor.getCount() > 0;
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
