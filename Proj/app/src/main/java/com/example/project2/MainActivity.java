package com.example.project2;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.graphics.Color;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity {
    private int mPosition;
    private ActivityResultLauncher<Intent> mActivityResultLauncher;
    private int selectID = -1;

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.miDis:{
                if (selectID == -1) {
                    AlertDialog.Builder infoDialog = new AlertDialog.Builder(MainActivity.this);
                    infoDialog.setTitle("Предупреждение");
                    infoDialog.setMessage("Пожалуйста, выберите студента, которого хотите просмотреть");
                    infoDialog.setCancelable(false);
                    infoDialog.setPositiveButton("Прочитано", null);
                    infoDialog.show();
                    return true;
                }
                else{
                    Intent intent1 = new Intent(MainActivity.this, StudentInfoActivity.class);
                    intent1.putExtra("student",mStudents.get((int)selectID));
                    mPosition=(selectID);
                    mActivityResultLauncher.launch(intent1);
                    //startActivityForResult(intent1, 1);
                    return true;

                }
            }
            /*case R.id.miAdd: {
                AlertDialog.Builder inputDialog = new AlertDialog.Builder(MainActivity.this);
                inputDialog.setTitle("Информация о студенте");
                inputDialog.setCancelable(false);
                View vv = (LinearLayout) getLayoutInflater().inflate(R.layout.student_dialog, null);
                inputDialog.setView(vv);
                final EditText mFIO = vv.findViewById(R.id.editFIO);
                final EditText mGroup = vv.findViewById(R.id.editGroup);
                final EditText mFacul = vv.findViewById(R.id.editFacultet);

                inputDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mStudents.add(new Student(
                                mFIO.getText().toString(),
                                mFacul.getText().toString(),
                                mGroup.getText().toString()
                        ));
                        mStudent_list_adapter.notifyDataSetChanged();
                    }
                })
                        .setNegativeButton("Отмена", null);
                inputDialog.show();
                return true;
            }*/
            case R.id.miAdd:{
                mStudents.add(new Student("", "", "", ""));

                //mStudent_list_adapter.notifyDataSetChanged();
                selectID = mStudents.size() - 1;

                Intent intent1 = new Intent(MainActivity.this, StudentInfoActivity.class);
                intent1.putExtra("student",mStudents.get(selectID));
                mPosition=(selectID);
                mActivityResultLauncher.launch(intent1);

                BackgroundTask backgroundTask = new BackgroundTask(this);
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                //backgroundTask.execute("add_info", gson.toJson(mStudents.get(selectID)));

                //mStudent_list_adapter.notifyDataSetChanged();
                //startActivityForResult(intent1, 1);
                return true;
            }
            case R.id.miChange: {
                if (selectID == -1) {
                    AlertDialog.Builder infoDialog = new AlertDialog.Builder(MainActivity.this);
                    infoDialog.setTitle("Предупреждение");
                    infoDialog.setMessage("Пожалуйста, выберите студента, которого хотите изменить");
                    infoDialog.setCancelable(false);
                    infoDialog.setPositiveButton("Прочитано", null);
                    infoDialog.show();
                    return true;
                }
                /*else {
                    ListView listview = findViewById(R.id.lvASI_Subjects);
                    //View subj = listview.getChildAt((int)selectID);
                    AlertDialog.Builder inputDialog = new AlertDialog.Builder(MainActivity.this);
                    inputDialog.setTitle("Информация о студенте");
                    inputDialog.setCancelable(false);
                    View vv = (LinearLayout) getLayoutInflater().inflate(R.layout.student_dialog, null);
                    inputDialog.setView(vv);
                    final EditText mFIO = vv.findViewById(R.id.editFIO);
                    final EditText mGroup = vv.findViewById(R.id.editGroup);
                    final EditText mFacul = vv.findViewById(R.id.editFacultet);

                    inputDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Student st = mStudents.get((int) selectID);
                            st.setFIO(mFIO.getText().toString());
                            st.setFacultet(mFacul.getText().toString());
                            st.setGroupe(mGroup.getText().toString());

                        }
                    })
                            .setNegativeButton("Отмена", null);
                    inputDialog.show();
                    return true;
                }*/
                else {
                    Intent intent1 = new Intent(MainActivity.this, StudentInfoActivity.class);
                    intent1.putExtra("student",mStudents.get(selectID));
                    mPosition=(selectID);
                    mActivityResultLauncher.launch(intent1);
                    //startActivityForResult(intent1, 1);
                    mStudent_list_adapter.notifyDataSetChanged();
                    return true;
                }
            }
            case R.id.miDelete: {
                if (selectID == -1) {
                    AlertDialog.Builder infoDialog = new AlertDialog.Builder(MainActivity.this);
                    infoDialog.setTitle("Предупреждение");
                    infoDialog.setMessage("Пожалуйста, выберите студента, которого хотите удалить");
                    infoDialog.setCancelable(false);
                    infoDialog.setPositiveButton("Прочитано", null);
                    infoDialog.show();
                    return true;
                } else {
                    AlertDialog.Builder deleteDialog = new AlertDialog.Builder(MainActivity.this);
                    deleteDialog.setTitle("Удалить студента\"" + mStudents.get(selectID).getFIO() + "\"?");

                    deleteDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteData(mStudents.get(selectID));
                            mStudents.remove(selectID);
                            mStudent_list_adapter.notifyDataSetChanged();
                            selectID = -1;
                        }
                    })
                            .setNegativeButton("Нет", null);
                    deleteDialog.show();
                    return true;
                }
            }
            case R.id.miAbout:{
                AlertDialog.Builder infoDialog = new AlertDialog.Builder(MainActivity.this);
                infoDialog.setTitle("О программе");
                infoDialog.setMessage("Это программа группы ПМ.1");
                infoDialog.setCancelable(false);
                infoDialog.setPositiveButton("Прочитано", null);
                infoDialog.show();
                return true;
            }
            case R.id.miExit:{
                finish();
                return true;
            }
            default:{}
        }
        return super.onOptionsItemSelected(item);
    }

    /*@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_new);
        ((LinearLayout) findViewById(R.id.llInput)).setVisibility(((Button) findViewById(R.id.bAddStudent)).getVisibility());

        mActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode()== Activity.RESULT_OK){
                            Intent intent = result.getData();
                            Student s = intent.getParcelableExtra("student");
                            mStudents.set(mPosition,s);
                            Toast.makeText(getApplicationContext(),
                                    "Студент :"+s.toString()+"\nУспешно сохранен",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                }
        );
    }*/

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_new);
        mStudents = new ArrayList<>();

        /////
        /*
        SharedPreferences sPref = getPreferences(MODE_PRIVATE);
        int size = sPref.getInt("count", 0);
        if(size>0){
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            for(int i =0; i <size; ++i){
                String s = sPref.getString("student"+i, "");
                if(!TextUtils.isEmpty(s)){
                    Student st = gson.fromJson(s,Student.class);
                    mStudents.add(st);
                }
            }
            createStudentList(null);
        }

         */
        //////
        mStudent_list_adapter=new student_list_adapter(mStudents,this);
        ((ListView) findViewById(R.id.lvlist2)).setAdapter(mStudent_list_adapter);

        loadStudentsFromDB();

        ListView listview = findViewById(R.id.lvlist2);
        listview.setAdapter(mStudent_list_adapter);


        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                selectID = position;
                ((TextView) findViewById(R.id.tvElementFIO)).setTextColor(Color.GRAY);

                String facultet = mStudents.get(position).getFacultet();
                for (Student s : mStudents) {

                    int index = mStudents.indexOf(s);
                    View student = listview.getChildAt(index);
                    ((TextView) student.findViewById(R.id.tvElementFIO)).setTextColor(Color.GRAY);

                    if (s.getFacultet().equals(facultet)) {
                        ((TextView) student.findViewById(R.id.tvElementFIO)).setTextColor(
                                mStudent_list_adapter.mContext.getResources().getColor(R.color.purple_700)
                        );
                    }
                }
                View sub1 = listview.getChildAt((int)selectID);
                ((TextView) sub1.findViewById(R.id.tvElementFIO)).setTextColor(mStudent_list_adapter.mContext.getResources().getColor(R.color.videl));

            }
        });
        AdapterView.OnItemLongClickListener clStudent= new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                String phone = mStudents.get(position).getNumPhone();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                startActivity(intent);
                return false;
            }
        };
        ((ListView) findViewById(R.id.lvlist2)).setOnItemLongClickListener(clStudent);

        mActivityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                new ActivityResultCallback<ActivityResult>() {
                    @Override
                    public void onActivityResult(ActivityResult result) {
                        if (result.getResultCode()== Activity.RESULT_OK){
                            Intent intent = result.getData();
                            Student s = intent.getParcelableExtra("student");

                                mStudents.set(mPosition,s);
                                Toast.makeText(getApplicationContext(),
                                        "Студент :"+s.toString()+"\nУспешно сохранен",
                                        Toast.LENGTH_SHORT).show();


                            mStudent_list_adapter.notifyDataSetChanged();
                        }
                    }
                }
        );

    }


    public static ArrayList<Student> mStudents;
    public static student_list_adapter mStudent_list_adapter;

    public void createStudentList(View view) {
        //mStudents = new ArrayList<>();
        //mStudent_list_adapter=new student_list_adapter(mStudents,this);
        //((ListView) findViewById(R.id.lvlist2)).setAdapter(mStudent_list_adapter);
        //mStudents.add(new Student("cat", "meow", "7"));
        ListView listview = findViewById(R.id.lvlist2);
        listview.setAdapter(mStudent_list_adapter);
        //mStudent_list_adapter.notifyDataSetChanged();
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                selectID = position;
                ((TextView) findViewById(R.id.tvElementFIO)).setTextColor(Color.GRAY);

                String facultet = mStudents.get(position).getFacultet();
                for (Student s : mStudents) {

                    int index = mStudents.indexOf(s);
                    View student = listview.getChildAt(index);
                    ((TextView) student.findViewById(R.id.tvElementFIO)).setTextColor(Color.GRAY);

                    if (s.getFacultet().equals(facultet)) {
                        ((TextView) student.findViewById(R.id.tvElementFIO)).setTextColor(
                                mStudent_list_adapter.mContext.getResources().getColor(R.color.purple_700)
                        );
                    }
                }
                View sub1 = listview.getChildAt((int)selectID);
                ((TextView) sub1.findViewById(R.id.tvElementFIO)).setTextColor(mStudent_list_adapter.mContext.getResources().getColor(R.color.videl));

            }
        });
        AdapterView.OnItemLongClickListener clStudent= new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View itemClicked, int position, long id) {
                String phone = mStudents.get(position).getNumPhone();
                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
                startActivity(intent);
                return false;
            }
        };
        ((ListView) findViewById(R.id.lvlist2)).setOnItemLongClickListener(clStudent);
    }

    public void addStudent(View view) {
        mStudents.add(new Student(
                ((EditText) findViewById(R.id.editFIO)).getText().toString(),
                ((EditText) findViewById(R.id.editFacultet)).getText().toString(),
                ((EditText) findViewById(R.id.editGroup)).getText().toString(),
                ((EditText) findViewById(R.id.editNumPhone)).getText().toString()
        ));
        ((EditText) findViewById(R.id.editFIO)).setText("");
        ((EditText) findViewById(R.id.editFacultet)).setText("");
        ((EditText) findViewById(R.id.editGroup)).setText("");
        ((EditText) findViewById(R.id.editNumPhone)).setText("");
        mStudent_list_adapter.notifyDataSetChanged();
    }
    @Override
    protected void onDestroy(){
        /*if (mStudents!=null){
            SharedPreferences.Editor ed = getPreferences(MODE_PRIVATE).edit();
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            ed.putInt("count", mStudents.size());
            for(int i =0; i <mStudents.size(); ++i){
                String s = gson.toJson(mStudents.get(i));
                ed.putString("student"+i, s);
            }
            ed.commit();
        }*/

        if (mStudents != null){
            for(Student student: mStudents) {
                System.out.println("saveData");
                saveData(student);
            }
        }
        super.onDestroy();
    }

    public void loadStudentsFromDB() {
        mStudents.clear();
        BackgroundTask backgroundTask = new BackgroundTask(this);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        backgroundTask.execute("get_students");
        mStudent_list_adapter.notifyDataSetChanged();
    }
    // TODO сохранить общую информацию о студенте
    public void saveData(Student student) {
        // проверка на существование записи о студенте
        // если есть запись, то изменить студента
        // если нет то этот блок
        System.out.println("student " + student);
        BackgroundTask backgroundTask = new BackgroundTask(this);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        backgroundTask.execute("add_info", gson.toJson(student));

    }

    public void deleteData(Student student) {
        BackgroundTask backgroundTask = new BackgroundTask(this);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        backgroundTask.execute("delete_student", gson.toJson(student));
    }

}