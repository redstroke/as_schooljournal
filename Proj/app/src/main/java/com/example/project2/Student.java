package com.example.project2;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Student implements Parcelable {
    private int id;
    private String mFIO;
    private String mFacultet;
    private String mGroupe;
    private String mNumPhone;
    private ArrayList<Subject> mSubjects;

    protected Student(Parcel in) {
        mFIO = in.readString();
        mFacultet = in.readString();
        mGroupe = in.readString();
        mNumPhone = in.readString();
        mSubjects = in.createTypedArrayList(Subject.CREATOR);
        id = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mFIO);
        dest.writeString(mFacultet);
        dest.writeString(mGroupe);
        dest.writeString(mNumPhone);
        dest.writeTypedList(mSubjects);
        dest.writeInt(id);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Student> CREATOR = new Creator<Student>() {
        @Override
        public Student createFromParcel(Parcel in) {
            return new Student(in);
        }

        @Override
        public Student[] newArray(int size) {
            return new Student[size];
        }
    };

    public String getFIO() {
        return mFIO;
    }

    public void setFIO(String FIO) {
        mFIO = FIO;
    }

    public String getNumPhone() {
        return mNumPhone;
    }

    public void setNumPhone(String NumPhone) { mNumPhone = NumPhone; }

    public String getFacultet() {
        return mFacultet;
    }

    public void setFacultet(String facultet) {
        mFacultet = facultet;
    }

    public String getGroupe() {
        return mGroupe;
    }

    public void setGroupe(String groupe) {
        mGroupe = groupe;
    }

    public Student(String FIO, String facultet, String groupe, String NumPhone) {
        mFIO = FIO;
        mFacultet = facultet;
        mGroupe = groupe;
        mNumPhone = NumPhone;
        mSubjects = new ArrayList<>();
        id = -1;
    }

    public ArrayList<Subject> getSubjects() {
        return mSubjects;
    }

    public void setSubjects(ArrayList<Subject> subjects) {
        mSubjects = subjects;
    }

    public int addSubject(Subject subject){
        mSubjects.add(subject);
        return mSubjects.size();
    }

    public Subject getSubject(int position){
        return mSubjects.get(position);
    }

    @Override
    public String toString() {
        return "Student{" +
                "mFIO='" + mFIO + '\'' +
                ", mFacultet='" + mFacultet + '\'' +
                ", mGroupe='" + mGroupe + '\'' +
                ", mNumPhone='" + mNumPhone + '\'' +
                '}';
    }

    public static final class StudentContract {
        // Student - Faculty N:1
        public static abstract class StudentEntry {
            public static final String ID = "id";
            public static final String FACULTY = "faculty";
            public static final String GROUP = "student_group";
            public static final String TELEPHONE = "telephone";
            public static final String FIO = "fio";
            // JSON-строка
            public static final String SUBJECTS = "subjects";
            public static final String TABLE_NAME = "student_table";
        }
        // N : N connection
       /* public static abstract class StudentSubjectEntry {
            public static final String ID_STUDENT = "id_student";
            public static final String ID_SUBJECT = "id_subject";
            public static final String
        }*/
    }
}

