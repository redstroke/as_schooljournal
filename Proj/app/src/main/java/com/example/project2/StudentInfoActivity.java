package com.example.project2;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class StudentInfoActivity extends AppCompatActivity implements SubjectListAdapter.SetMark {
    private Student s;
    private SubjectListAdapter mSubjectListAdapter;
    private student_list_adapter mStudent_list_adapter;
    private int selectID = -1;


    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.subject_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.miAdd: {
                AlertDialog.Builder inputDialog = new AlertDialog.Builder(StudentInfoActivity.this);
                inputDialog.setTitle("Информация о дисциплине");
                inputDialog.setCancelable(false);
                View vv = (LinearLayout) getLayoutInflater().inflate(R.layout.subject_dialog, null);
                inputDialog.setView(vv);
                final EditText mName = vv.findViewById(R.id.editSD_Name);
                final Spinner mMark = vv.findViewById(R.id.sSD_mark);


                inputDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        s.addSubject(new Subject(
                                mName.getText().toString(),
                                Integer.parseInt(mMark.getSelectedItem().toString())
                        ));
                        mSubjectListAdapter.notifyDataSetChanged();
                    }
                })
                        .setNegativeButton("Отмена", null);
                inputDialog.show();
                return true;
            }
            case R.id.miChange: {
                if (selectID == -1) {
                    AlertDialog.Builder infoDialog = new AlertDialog.Builder(StudentInfoActivity.this);
                    infoDialog.setTitle("Предупреждение");
                    infoDialog.setMessage("Пожалуйста, выберите дисциплину, которую хотите изменить");
                    infoDialog.setCancelable(false);
                    infoDialog.setPositiveButton("Прочитано", null);
                    infoDialog.show();
                    return true;
                }
                else {
                    ListView listview = findViewById(R.id.lvASI_Subjects);
                    View subj = listview.getChildAt((int)selectID);
                    AlertDialog.Builder inputDialog = new AlertDialog.Builder(StudentInfoActivity.this);
                    inputDialog.setTitle("Информация о дисциплине");
                    inputDialog.setCancelable(false);
                    View vv = (LinearLayout) getLayoutInflater().inflate(R.layout.subject_dialog, null);
                    inputDialog.setView(vv);
                    Subject su = s.getSubject((int)selectID);
                    final EditText mName = vv.findViewById(R.id.editSD_Name);
                    final Spinner mMark = vv.findViewById(R.id.sSD_mark);
                    mName.setText(su.getName().toString());
                    //mMark.setPromptId(su.getMark());
                    int m = su.getMark();
                    mMark.setSelection(5-m);

                    inputDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ((SubjectListAdapter.SetMark) mSubjectListAdapter.mContext).setMark((int)selectID, Integer.parseInt(mMark.getSelectedItem().toString()));
                            //Subject su = s.getSubject((int)selectID);
                            su.setName(mName.getText().toString());

                        }
                    })
                            .setNegativeButton("Отмена", null);

                    inputDialog.show();
                    return true;
                }
            }
            case R.id.miDelete: {
                if (selectID == -1) {
                    AlertDialog.Builder infoDialog = new AlertDialog.Builder(StudentInfoActivity.this);
                    infoDialog.setTitle("Предупреждение");
                    infoDialog.setMessage("Пожалуйста, выберите дисциплину, которую хотите удалить");
                    infoDialog.setCancelable(false);
                    infoDialog.setPositiveButton("Прочитано", null);
                    infoDialog.show();
                    return true;
                } else {
                    AlertDialog.Builder deleteDialog = new AlertDialog.Builder(StudentInfoActivity.this);
                    deleteDialog.setTitle("Удалить дсциплину\"" + s.getSubjects().get((int) selectID).getName() + "\"?");

                    deleteDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            s.getSubjects().remove((int)selectID);
                            mSubjectListAdapter.notifyDataSetChanged();
                            selectID = -1;
                        }
                    })
                            .setNegativeButton("Нет", null);
                    deleteDialog.show();
                    return true;
                }
            }
            case R.id.miAbout:{
                AlertDialog.Builder infoDialog = new AlertDialog.Builder(StudentInfoActivity.this);
                infoDialog.setTitle("О программе");
                infoDialog.setMessage("Это программа группы ПМ.1");
                infoDialog.setCancelable(false);
                infoDialog.setPositiveButton("Прочитано", null);
                infoDialog.show();
                return true;
            }
            case R.id.miExit:{
                finish();
                return true;
            }
            default:{}
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_info);

        s = getIntent().getParcelableExtra("student");
        //((TextView) findViewById(R.id.tvASI_FIO)).setText(s.getFIO());
        //((TextView) findViewById(R.id.tvASI_Facultet)).setText(s.getFacultet());
        //((TextView) findViewById(R.id.tvASI_Group)).setText(s.getGroupe());

        ((EditText) findViewById(R.id.editFIO)).setText(s.getFIO());
        ((EditText) findViewById(R.id.editFacultet)).setText(s.getFacultet());
        ((EditText) findViewById(R.id.editGroup)).setText(s.getGroupe());
        ((EditText) findViewById(R.id.editNumPhone)).setText(s.getNumPhone());

        //int k = s.getSubjects().size();
        //for()

        mSubjectListAdapter=new SubjectListAdapter(s.getSubjects(), StudentInfoActivity.this);
        ((ListView) findViewById(R.id.lvASI_Subjects)).setAdapter(mSubjectListAdapter);

        ((ListView) findViewById(R.id.lvASI_Subjects)).setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener(){
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id){
                        AlertDialog.Builder deleteDialog = new AlertDialog.Builder(StudentInfoActivity.this);
                        deleteDialog.setTitle("Удалить дсциплину\"" + s.getSubjects().get(position).getName() + "\"?");

                        deleteDialog.setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                s.getSubjects().remove(position);
                                mSubjectListAdapter.notifyDataSetChanged();
                            }
                        })
                                .setNegativeButton("Нет", null);
                        deleteDialog.show();
                        return false;
                    }
                }
        );
        ListView listview = findViewById(R.id.lvASI_Subjects);
        ((ListView) findViewById(R.id.lvASI_Subjects)).setOnItemClickListener(
                new AdapterView.OnItemClickListener(){
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id){
                        if (selectID != -1){
                            int oldID = selectID;
                            selectID = position;
                            View sub1 = listview.getChildAt((int)selectID);
                            View sub2 = listview.getChildAt((int)oldID);
                            ((TextView) sub2.findViewById(R.id.tvElementSubject)).setTextColor(Color.GRAY);
                            ((TextView) sub1.findViewById(R.id.tvElementSubject)).setTextColor(mSubjectListAdapter.mContext.getResources().getColor(R.color.videl));
                        }
                        else{
                            selectID = position;
                            View sub1 = listview.getChildAt((int)selectID);
                            ((TextView) sub1.findViewById(R.id.tvElementSubject)).setTextColor(mSubjectListAdapter.mContext.getResources().getColor(R.color.videl));
                        }


                    }
                }
        );

    }


    private void showPopupMenu(View view, int position){
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.inflate(R.menu.popup_menu);
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Integer mark;
                switch (item.getItemId()){
                    case R.id.popup5: {mark = 5; break;}
                    case R.id.popup4: {mark = 4; break;}
                    case R.id.popup3: {mark = 3; break;}
                    case R.id.popup2: {mark = 2; break;}
                    default:
                        return false;
                }
                if ((s!=null)&&(position<s.getSubjects().size())){
                    s.getSubjects().get(position).setMark(mark);
                    mSubjectListAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });
        popupMenu.show();
    }

    /*public void AddSubject(View view) {
        s.addSubject(new Subject(
                ((TextView) findViewById(R.id.editASI_Subject)).getText().toString(),
                Integer.parseInt(((Spinner) findViewById(R.id.sASI_mark)).getSelectedItem().toString())));
        mSubjectListAdapter.notifyDataSetChanged();
    }*/

    public void clSave(View view) {
        //((EditText) findViewById(R.id.editFIO)).setText(s.getFIO());
        //((EditText) findViewById(R.id.editFacultet)).setText(s.getFacultet());
        //((EditText) findViewById(R.id.editGroup)).setText(s.getGroupe());
        final EditText mFIO = findViewById(R.id.editFIO);
        final EditText mGroup = findViewById(R.id.editGroup);
        final EditText mFacul = findViewById(R.id.editFacultet);
        final EditText mNumPhone = findViewById(R.id.editNumPhone);
        s.setFIO(mFIO.getText().toString());
        s.setGroupe(mGroup.getText().toString());
        s.setFacultet(mFacul.getText().toString());
        s.setNumPhone(mNumPhone.getText().toString());
        System.out.println("student " + s);

        //BackgroundTask backgroundTask = new BackgroundTask(this);
        //GsonBuilder builder = new GsonBuilder();
        //Gson gson = builder.create();
        //backgroundTask.execute("add_info", gson.toJson(s));

        Intent intent = new Intent();
        intent.putExtra("student",s);
        setResult(RESULT_OK, intent);
        //onActivityResult(1, RESULT_OK, intent );
        finish();
    }

    public void clExit(View view) {
        finish();
    }

    @Override
    public void onBackPressed(){
        AlertDialog.Builder quitDialog = new AlertDialog.Builder(this);
        quitDialog.setTitle("Закончить?");
        quitDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(TextUtils.isEmpty(((EditText) findViewById(R.id.editFIO)).getText().toString())){
                    ((EditText) findViewById(R.id.editFIO)). setError("Укажите ФИО");
                    return;
                }
                if(TextUtils.isEmpty(((EditText) findViewById(R.id.editNumPhone)).getText().toString())){
                    ((EditText) findViewById(R.id.editNumPhone)). setError("Укажите номер телефона");
                    return;
                }
                if(TextUtils.isEmpty(((EditText) findViewById(R.id.editFacultet)).getText().toString())){
                    ((EditText) findViewById(R.id.editFacultet)). setError("Укажите факультете");
                    return;
                }
                if(TextUtils.isEmpty(((EditText) findViewById(R.id.editGroup)).getText().toString())){
                    ((EditText) findViewById(R.id.editGroup)). setError("Укажите группу");
                    return;
                }
                clSave(null);
            }

        })
                .setNeutralButton("Отмена", null)
                .setNegativeButton("Не сохранять", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clExit(null);
                    }
                });
        quitDialog.show();
    }

    @Override
    public void setMark(int position, Integer mark) {
        if((s!=null) && (position < s.getSubjects().size())){
            s.getSubjects().get(position).setMark(mark);
            mSubjectListAdapter.notifyDataSetChanged();

        }
    }

    public void clNewSubject(View view) {
        AlertDialog.Builder inputDialog = new AlertDialog.Builder(StudentInfoActivity.this);
        inputDialog.setTitle("Информация о дисциплине");
        inputDialog.setCancelable(false);
        View vv = (LinearLayout) getLayoutInflater().inflate(R.layout.subject_dialog, null);
        inputDialog.setView(vv);
        final EditText mName = vv.findViewById(R.id.editSD_Name);
        final Spinner mMark = vv.findViewById(R.id.sSD_mark);

        inputDialog.setPositiveButton("Сохранить", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                s.addSubject(new Subject(
                        mName.getText().toString(),
                        Integer.parseInt(mMark.getSelectedItem().toString())
                ));
                mSubjectListAdapter.notifyDataSetChanged();
            }
        })
                .setNegativeButton("Отмена", null);
        inputDialog.show();
    }
}