package com.example.project2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.ArrayList;

public class SubjectListAdapter extends BaseAdapter {
    ArrayList<Subject> mSubjects = new ArrayList<>();
    Context mContext;
    LayoutInflater mInflater;

    interface SetMark{
        void setMark(int position, Integer mark);
    }
    interface SetName{
        void setName(int position, String name);
    }

    public SubjectListAdapter(ArrayList<Subject> subjects, Context context) {
        mSubjects = subjects;
        mContext = context;
        mInflater= (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mSubjects.size();
    }

    @Override
    public Object getItem(int position) {
        return mSubjects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.subject_element,parent,false);
        if(mSubjects.isEmpty()) return convertView;

        ((TextView) convertView.findViewById(R.id.tvElementSubject)).setText(mSubjects.get(position).getName());
        ((TextView) convertView.findViewById(R.id.tvElementMark)).setText(mSubjects.get(position).getMark().toString());

        ((TextView) convertView.findViewById(R.id.tvElementMark)).setOnClickListener(
                new View.OnClickListener(){
                    @Override
                    public void onClick(View v){
                        PopupMenu popupMenu = new PopupMenu((StudentInfoActivity) mContext, v);
                        popupMenu.inflate(R.menu.popup_menu);
                        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                            @Override
                            public boolean onMenuItemClick(MenuItem item) {
                                Integer mark;
                                switch (item.getItemId()){
                                    case R.id.popup5: {mark = 5; break;}
                                    case R.id.popup4: {mark = 4; break;}
                                    case R.id.popup3: {mark = 3; break;}
                                    case R.id.popup2: {mark = 2; break;}
                                    default:
                                        return false;
                                }
                                ((SetMark) mContext).setMark(position, mark);

                                return true;
                            }
                        });
                        popupMenu.show();
                    }
                }
        );


        return convertView;
    }
}
