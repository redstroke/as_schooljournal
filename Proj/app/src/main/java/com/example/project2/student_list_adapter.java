package com.example.project2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class student_list_adapter extends BaseAdapter {

    ArrayList<Student> mStudents;
    Context mContext;
    LayoutInflater mInflater;

    public student_list_adapter(ArrayList<Student> students, Context context) {
        mStudents = students;
        mContext = context;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mStudents.size();
    }

    @Override
    public Object getItem(int position) {
        return mStudents.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = mInflater.inflate(R.layout.student_element,parent,false);
        if(mStudents.isEmpty()) return convertView;

        ((TextView) convertView.findViewById(R.id.tvElementFIO)).setText(mStudents.get(position).getFIO());
        ((TextView) convertView.findViewById(R.id.tvElementFacultet)).setText(mStudents.get(position).getFacultet());
        ((TextView) convertView.findViewById(R.id.tvElementGroup)).setText(mStudents.get(position).getGroupe());
        ((TextView) convertView.findViewById(R.id.tvElementNumPhone)).setText(mStudents.get(position).getNumPhone());

        //if(position%2==1) ((LinearLayout) convertView.findViewById(R.id.llelement)).setBackgroundColor(
        //        mContext.getResources().getColor(R.color.purple_200)
        //);

        return convertView;
    }
}
